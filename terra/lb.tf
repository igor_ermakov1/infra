data "vkcs_networking_network" "extnet" {
  name = "internet"
}

# создаем сеть с именем «lb»
resource "vkcs_networking_network" "lb" {   
  name = "network"
}

# подсеть из сети. В примере: sublb
resource "vkcs_networking_subnet" "sublb" {
  name = "subnet"
  cidr = "192.168.199.0/24"
  network_id = "${vkcs_networking_network.lb.id}"
}

# роутер для внешней сети и взаимодействия с внешним миром
resource "vkcs_networking_router" "router" {
   name                = "router"
   admin_state_up      = true
  #external_network_id = resource.vkcs_networking_network.lb.id
  external_network_id = data.vkcs_networking_network.extnet.id
}

# подключаем роутер к внутренней сети.
resource "vkcs_networking_router_interface" "db" {
   router_id = vkcs_networking_router.router.id
   subnet_id = vkcs_networking_subnet.sublb.id
}

# ********* создаем балансировщик ************* 
data "vkcs_images_image" "compute" {
   name = "Ubuntu-22.04-202208"
}

data "vkcs_compute_flavor" "compute" {
  name = "STD2-1-1"
}

# ************** создаем серверы **************
resource "vkcs_compute_instance" "compute_1" {
  name                    = "worker01"
  flavor_id               = data.vkcs_compute_flavor.compute.id
  key_pair                = "id_rsa_vlcloud"
#  security_groups         = ["default","ssh"]
  security_groups         = ["default"]
  availability_zone       = "MS1"

  block_device {
    uuid                  = data.vkcs_images_image.compute.id
    source_type           = "image"
    destination_type      = "volume"
    volume_type           = "ceph-ssd"
    volume_size           = 8
    boot_index            = 0
    delete_on_termination = true
  }

  network {
    uuid = vkcs_networking_network.lb.id
    fixed_ip_v4 = "192.168.199.110"
  }

  depends_on = [
    vkcs_networking_network.lb,
    vkcs_networking_subnet.sublb
  ]
}

# **********
resource "vkcs_compute_instance" "compute_2" {
  name                    = "worker02"
  flavor_id               = data.vkcs_compute_flavor.compute.id
  key_pair                = "id_rsa_vlcloud"
#  security_groups         = ["default","ssh"]
  security_groups         = ["default"]
  availability_zone       = "MS1"

  block_device {
    uuid                  = data.vkcs_images_image.compute.id
    source_type           = "image"
    destination_type      = "volume"
    volume_type           = "ceph-ssd"
    volume_size           = 8
    boot_index            = 0
    delete_on_termination = true
  }

  network {
    uuid = vkcs_networking_network.lb.id
    fixed_ip_v4 = "192.168.199.111"
  }

  depends_on = [
    #vkcs_networking_network.network,
    #vkcs_networking_subnet.subnetwork
    vkcs_networking_network.lb,
    vkcs_networking_subnet.sublb
  ]
}

# *****************

resource "vkcs_lb_loadbalancer" "loadbalancer" {
  name = "loadbalancer"
  vip_subnet_id = "${vkcs_networking_subnet.sublb.id}"
  tags = ["tag1"]
}

resource "vkcs_lb_listener" "listener" {
  name = "listener"
  protocol = "HTTP"
  protocol_port = 8080
  loadbalancer_id = "${vkcs_lb_loadbalancer.loadbalancer.id}"
}

resource "vkcs_lb_pool" "pool" {
  name = "pool"
  protocol = "HTTP"
  lb_method = "ROUND_ROBIN"
  listener_id = "${vkcs_lb_listener.listener.id}"
}

resource "vkcs_lb_member" "member_1" {
  address = "192.168.199.110"
  protocol_port = 8080
  pool_id = "${vkcs_lb_pool.pool.id}"
  subnet_id = "${vkcs_networking_subnet.sublb.id}"
  weight = 0
}

resource "vkcs_lb_member" "member_2" {
  address = "192.168.199.111"
  protocol_port = 8080
  pool_id = "${vkcs_lb_pool.pool.id}"
  subnet_id = "${vkcs_networking_subnet.sublb.id}"
}