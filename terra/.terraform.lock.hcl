# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/vk-cs/vkcs" {
  version     = "0.5.5"
  constraints = "~> 0.5.2"
  hashes = [
    "h1:DyznNZjuKQ+SPBihjmm2scYADFJxT8WhwDAme7W7mp4=",
  ]
}
